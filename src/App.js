import React, { useState } from "react";
import "./App.css";
import City from "./components/City";
import Weather from "./components/Weather";

let city = "oslo";


// Component to get the city name.
// Component to get and display weather.
// Component to display a map.

const App = () => {
  const [city, setCity] = useState("");
  console.log(city);

  const handleCityClicked = (result) => {
    console.log("From City: ", result);
    setCity(result);
  };
  return (
    <div className="App">
      <City click={handleCityClicked} />
      <Weather city={city} />
    </div>
  );
};

export default App;
