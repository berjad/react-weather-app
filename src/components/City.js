import React, { useState } from 'react';

const City = (props) => {

    const [city, setCity] = useState('');
    console.log(city);
    

    const onCityClicked = (ev) => {
        console.log("Event: ", ev);

        props.click(city)
        
    }

    const onCityChanged = (ev) => {

        setCity(ev.target.value);

    }

    return (
        <form>
            <div>
                <label>City: </label>
                <input type="text" name="city" id="city" placeholder="Select a city" onChange={onCityChanged}/>
            </div>
            <div>
                <button type="button" onClick={onCityClicked}>Send</button>
            </div>
        </form>
    
        )

}

export default City;

