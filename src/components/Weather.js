import React, { useEffect } from "react";

const Weather = (props) => {
  const API_KEY = "9721686f3d39f5b254428b074f1412bf";
  const URL = `https://api.openweathermap.org/data/2.5/weather?&units=metric&q=${props.city}&appid=${API_KEY}`;
  const fetchOption = {
    method: "GET",
    mode: "no-cors",
    headers: {
      "Content-Type": "application/json",
    },
  };

  console.log(props.city);

  useEffect(() => {
    if (props.city !== "") {
      fetch(URL, fetchOption)
        .then((r) => r.json())
        .then((resp) => console.log(resp))
        .catch(er => console.error(er))
    }
  }, [props.city]);

  return (
    <div>
      <h1>Weather for {props.city}</h1>
    </div>
  );
};

export default Weather;
